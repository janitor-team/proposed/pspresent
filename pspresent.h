/*
   pspresent: PostScript presentation tool
   Copyright (C) Matthew Chapman 2001-2004

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>

#define VERSION "1.3"

struct pspage {
	char *data;
	int delay; /* delay before going to next slide */
	int is_last; /* Last in a series of overlays */
};

enum Orientation
{
	Portrait = 0,
	Landscape = 90,
	UpsideDown = 180,
	Seascape = 270
};

#define MWM_HINTS_DECORATIONS		(1L << 1)
typedef struct
{
	unsigned long flags;
	unsigned long functions;
	unsigned long decorations;
	long inputMode;
	unsigned long status;
} PropMotifWmHints;

/* ps.c */
Bool PSScanDocument(char *start, char *end, int bounds[4], int *orientation,
		    int *num_pages, struct pspage **page_offsets,
		    int *num_real_pages, int **real_page_offsets);
Bool PSGetOrientation(char *string, int *orientation);

/* gs.c */
int GSStart(Window draw_wnd, Pixmap draw_pix, int wnd_width, int wnd_height,
		int bounds[4], int orientation);
char *GSWrite(int gs_fd, char *start, char *end);
void GSStop(int gs_fd);
void GSNextPage(void);
Bool GSProcessMessage(XClientMessageEvent *message);

