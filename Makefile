#
# pspresent: PostScript presentation tool
# Copyright (C) Matthew Chapman 2001-2003
#

# You may need to change these paths
X11_CFLAGS=-I/usr/X11R6/include
X11_LDLIBS=-L/usr/X11R6/lib -lX11

# Remove the following two lines to disable XINERAMA support
XINERAMA_CFLAGS=-DHAVE_LIBXINERAMA
XINERAMA_LDLIBS=-lXinerama

CC = gcc
CFLAGS = -Wall -O2 $(X11_CFLAGS) $(XINERAMA_CFLAGS)
LDLIBS = $(X11_LDLIBS) $(XINERAMA_LDLIBS)
LDFLAGS ?= 

TARGET = pspresent
OBJS = pspresent.o gs.o ps.o

$(TARGET): $(OBJS)
	$(CC) -o $(TARGET) $(OBJS) $(LDFLAGS) $(LDLIBS) 

clean:
	rm -f $(TARGET) $(OBJS)

.SUFFIXES:
.SUFFIXES: .c .o

.c.o:
	$(CC) $(CFLAGS) -o $@ -c $<
